Changelog
=========

1.2.2: 10/23/2015
-----------------
*Affected files: run_bot.py and ReputationManager.py*

* Fixed error in run_bot.py with importing testing config.
* Cleaned up function that checks if a user's account is new.
* When the bot replies to an !offer or it will only give the money amount offered in it's reply.

1.2.1: 10/18/2015
-----------------
*Affected files: ReputationManager.py*

* Fixed error where multiple !successes could be used and their values be added to users profile.

1.2.0: 10/18/2015
-----------------
*Affected files: ReputationManager.py, DatabaseManager, run_bot.py, and templates.py*

* Completely got rid of review system (wasn't very useful).
* Got rid of completions column and replaced with retractions
    - The new file migrate_db.py changes the database table to fit with this new schema.
* Added a run_bot.py argument `-m` or `--migrate` to run a new database migration to change the schema of the database as updates come and may require it.
* Two new commands !cancel and !retract.
    - They both do the same thing internally, but !cancel makes more sense comming from the OP and !retract comming from the offerer.

1.0.5: 10/16/2015
----------------------
*Affected files: ReputationManager.py, run_bot.py, and templates.py*

* Started keeping a changelog
* Fixed bug if !accept was never used and the bot would get stuck on !success
    - Just pulls the username from the !offer if it can. If it can't it will tell the user to reply to the !offer or use !accept and then try !success again.
* Fixed bug where the bot would reply to itself if a ![command] was used in one of its replies.
* If a user replies to the bots user info comment with !accept it will pull the info from the !offer comment the bot replied to.
* Fixed typo in templates.py user warning.