import re
import sys
import time
import math
import praw
import logging
import traceback
import templates
import OAuth2Util
from datetime import datetime, timedelta
from DatabaseManager import DatabaseManager


class ReputationManager:


    def __init__(self, subreddit, username, db_name, debug=False):

        '''
            initializer for the ReputationManager
            pre:  the subreddit it's going to manage in, and the name of the database
                  and if debug mode is turned on or not, default is False
            post: the ReputationManager class is initialized
        '''

        user_agent = 'Reputation-Manager v1.2.2 /u/cutety'
        self.r = praw.Reddit(user_agent=user_agent,log_requests=0)
        self.o = OAuth2Util.OAuth2Util(self.r, print_log=False)
        self.subreddit = subreddit
        self.username = username
        print('Initializing database...')
        self.db = DatabaseManager(db_name)

        # the functions are associated with the identifiers position in this list
        # this is done so identifier can be changed at anytime and the same function
        # will be done without having to change any other code
        self.identifiers = ['!offer', '!accept', '!success', '!scam', '!report',
                            '!cancel', '!retract']

        # configure logging
        logging.basicConfig(filename='REPMLog.log',
                            level=logging.DEBUG,
                            format='%(asctime)s %(message)s')
        self.debug = debug

    def mainLoop(self):

        '''
            the main loop for running the bot
            pre:  None
            post: The bot is running
        '''

        while True:
            try:
                self.checkForComments()
            except Exception as e:
                for frame in traceback.extract_tb(sys.exc_info()[2]):
                    fname,lineno,fn,text = frame
                    logging.debug('ERROR: {} on line {} in {}() in file{}'.format(e, lineno, fn, fname))
                time.sleep(60)


    def checkForComments(self):

        '''
            starts checking the comments for identifiers
            pre:  None
            post: None
        '''

        print('Checking comments...')
        comment_stream = praw.helpers.comment_stream(self.r, subreddit=self.subreddit)
        for comment in comment_stream:
            self.o.refresh()
            for identifier in self.identifiers:
                if identifier in str(comment) and not self.db.hasBotAlreadyReplied(comment.id):
                    # ignore it if it's just the bots comment
                    if str(comment.author) == self.username:
                        continue
                    # Do the correct task for the identifer in the comment
                    print('Doing task: {}'.format(identifier))
                    self._doTask(self.identifiers.index(identifier), comment)


    # "Private" methods


    def _doTask(self, task, comment):

        '''
            switcher for doing a task
            pre:  the task it needs to do, and a praw comment object
            post: None
        '''

        if task == 0:
            self._offerMade(comment)
        elif task == 1:
            self._acceptMade(comment)
        elif task == 2:
            self._successOnDeal(comment)
        elif task == 3:
            self._scamOnDeal(comment)
        elif task == 4:
            self._reportUser(comment)
        elif task == 5 or task == 6:
            self._cancelDeal(comment)
        else:
            self._replyWithError(comment, 'Err')


    def _offerMade(self, comment):

        '''
            posts the users stats and creates the offer
            pre:  praw comment object
            post: replies to comment with the users stats
        '''

        offer = self._parseComment(comment, '!offer')
        # check if there was no offer attached to the statement
        if not offer:
            if not self.debug:
                comment.reply('''Please provide an offer with the !offer statement.
                                 \n\nMake another comment with an offer after the stament.''')
                self.db.addCommentToReplied(comment.id)
            else:
                print('''Please provide an offer with the !offer statement.
                        \n\nMake another comment with an offer after the stament.''')
        # then if an offer has already been accepted, let the user know
        # that this offer can't be used
        elif self.db.hasOfferBeenAccepted(comment.submission.id):
            if not self.debug:
                comment.reply('Sorry, an offer has already been accepted on this thread.')
                self.db.addCommentToReplied(comment.id)
            else:
                print('Sorry, an offer has already been accepted on this thread.')
        # Reply to the offer statement with the stats of the user offering
        else:
            reply = '{} has offered {}\n\nHere are the users stats:\n\n'.format(str(comment.author),
                                                                                offer)
            stats = self.db.getUserStats(str(comment.author))

            # add info to the reply
            reply += templates.USER_STATS.format(stats[0], stats[1],
                                                 stats[2], stats[3])
            # if the user is new, add a warning
            if self._shouldAddNewUserWarning(str(comment.author)):
                reply += templates.WARNING

            if not self.debug:
                comment.reply(reply)
                self.db.addCommentToReplied(comment.id)
            else:
                print(reply)


    def _acceptMade(self, comment):

        '''
            method for dealing with the op accepting
            pre:  praw comment object
            post: a thread entry is created with the offer, replies
                  with a confirmation
        '''

        author = str(comment.author)
        parent = self._getParentComment(comment)

        # check if it's in reply to the user info from the bot
        if str(parent.author) == self.username:
            # get the comment above it
            parent = self._getParentComment(parent)

        # parent will be returned as a submission object if it is a 
        # top level comment
        if type(parent) == praw.objects.Submission:
            if not self.debug:
                comment.reply('You can only use the accept command in reply to an offer.')
                self.db.addCommentToReplied(comment.id)
                return
            else:
                print('You can only use the accept command in reply to an offer.')
                return

         # Check to make sure the author is also the OP of the post
        if author != str(comment.submission.author):
            if not self.debug:
                comment.reply('Only the OP of the post can accept.')
                self.db.addCommentToReplied(comment.id)
                return
            else:
                print('Only the OP of the post can accept.')
                return
        
        # if the parent exists parse it to get the offer
        if parent:
            offer = self._parseComment(parent, '!offer')
            parent_author = str(parent.author)
        # if for some reason there is no parent, reply with an error
        else:
            self._replyWithError(comment, 'NoParent')
            return

        # create the thread entry for later closing
        self.db.createThreadEntry(comment.submission.id, parent_author, author, offer)

        if not self.debug:
            comment.reply(templates.ACCEPTED.format(author, parent_author, offer))
            self.db.addCommentToReplied(comment.id)
        else:
            print(templates.ACCEPTED.format(author, parent_author, offer))


    def _successOnDeal(self, comment):

        '''
            calls _completeDeal to complete a deal with 'success'
            pre:  praw comment object
            post: None
        '''

        if self.db.isThreadCompleted(comment.submission.id):
            if not self.debug:
                comment.reply('The thread is already completed and result recorded.')
                self.db.addCommentToReplied(comment.id)
                return
            else:
                print('The thread is already completed and result recorded.')
                return

        if str(comment.author) != str(comment.submission.author):
            if not self.debug:
                comment.reply('Only the OP of the post determine it\'s status.')
                self.db.addCommentToReplied(comment.id)
                return
            else:
                print('Only the OP of the post determine it\'s status.')
                return

        self._completeDeal(comment, 'success')


    def _scamOnDeal(self, comment):

        '''
            calls _completeDeal with 'scam' and reporsts user
            pre:  praw comment object
            post: None            
        '''

        if self.db.isThreadCompleted(comment.submission.id):
            if not self.debug:
                comment.reply('The thread is already completed and result recorded.')
                self.db.addCommentToReplied(comment.id)
                return
            else:
                print('The thread is already completed and result recorded.')
                return

        if str(comment.author) != str(comment.submission.author):
            comment.reply('Only the OP of the post determine it\'s status.')
            self.db.addCommentToReplied(comment.id)
            return

        self._completeDeal(comment, 'scam')
        self._reportUser(comment)
        # open the thread for offers again
        self.db.openThread(comment.submission.id)


    def _cancelDeal(self, comment):

        if self.db.isThreadCompleted(comment.submission.id):
            if not self.debug:
                comment.reply('The thread is already completed and result recorded.')
                self.db.addCommentToReplied(comment.id)
                return
            else:
                print('The thread is already completed and result recorded.')
                return

        offerer = self.db.getUserThatOffered(comment.submission.id)
        if not offerer:
            if not self.debug:
                comment.reply('No deal has been accepted to cancel.')
                self.db.addCommentToReplied(comment.id)
                return
            else:
                print('No deal has been accepted to cancel.')
                return
        else:
            if str(comment.author) != offerer[0] and str(comment.author) != str(comment.submission.author):
                if not self.debug:
                    comment.reply('Only the OP of the post or the accepted offerer can cancel the deal.')
                    self.db.addCommentToReplied(comment.id)
                    return
                else:
                    print('Only the OP of the post or the accepted offerer can cancel the deal.')
                    return
            else:
                self._completeDeal(comment, 'cancel')
                # open the thread for offers again
                self.db.openThread(comment.submission.id)


    def _completeDeal(self, comment, result):

        '''
            completes the deal on the thread based on the result
            pre:  praw comment object, and result of the thread
            post: the user is updated with the results and a confirmation
                  is replied 
        '''

        # check if the deal has already been completed
        if self.db.isThreadCompleted(str(comment.submission.id)):
            if not self.debug:
                comment.reply('The offer on this thread has already been completed.')
                self.db.addCommentToReplied(comment.id)
                return
            else:
                print('The offer on this thread has already been completed.')
                return

        # grab the user that offered
        user = self.db.getUserThatOffered(comment.submission.id)

        if user:
            # check if we need to switch the users around if the op is cancelling
            # so it will update their profile instead
            if result == 'cancel' and str(comment.author) == str(comment.submission.author):
                user = [str(comment.submission.author)]

        # if the OP never !accept the offer
        else:
            parent = self._getParentComment(comment)

            if type(parent) == praw.objects.Submission:
                if not self.debug:
                    comment.reply('''You didn\'t use the accept command and this is
                                     a top level comment, so I don't know who succeed.
                                     try replying to the offer instead.''')
                    self.db.addCommentToReplied(comment.id)
                    return
                else:
                    print('''You didn\'t use the accept command and this is
                             a top level comment, so I don't know who succeed.
                             try replying to the offer instead.''')
                    return
            # then check if it's an offer
            elif '!offer' in str(parent):
                # then set the user as the parents author
                user = [str(parent.author)]
                # we'll also need to make a thread entry
                self.db.createThreadEntry(comment.submission.id, user, 
                                          str(comment.author), self._parseComment(comment, '!offer'))
            else:
                if not self.debug:
                    comment.reply('''An accept was never made, and I can't find
                                     the user that offered. Please try replying,
                                     to the offer or accept the offer and use success
                                     again''')
                    self.db.addCommentToReplied(comment.id)
                    return
                else:
                    print('''An accept was never made, and I can't find
                             the user that offered. Please try replying,
                             to the offer or accept the offer and use success
                             again''')
                    return


        # add the finished results
        self.db.addFinishedResults(user[0], result)
        self.db.completeThread(comment.submission.id)

        if not self.debug:
            comment.reply(templates.COMPLETED[result].format(user[0]))
            self.db.addCommentToReplied(comment.id)
        else:
            print(templates.COMPLETED[result].format(user[0]))


    def _replyWithError(self, comment, err):

        '''
            replies to comment with corresponding error message
            pre:  praw comment object, error type
            post: the comment is replied to with the error message
        '''

        if not self.debug:
            comment.reply(templates.ERROR[err])
            self.db.addCommentToReplied(comment.id)
        else:
            print(templates.ERROR[err])


    def _reportUser(self, comment):

        '''
            a message is sent to the mods with a user report
            pre:  praw comment object
            post: message is sent to moderators of the sub with a report 
        '''

        body = self._parseComment(comment, '!report')
        body = body + '\n\n' if body else 'No description given\n\n'

        body += '''
        Reported by: {}\n
        Comment id: {}\n
        In thread: {}'''.format(str(comment.author), comment.id, comment.submission.permalink)
        if not self.debug:
            # Send a message to the mod mail
            self.r.send_message('/r/' + self.subreddit, 'User Report', body)
        else:
            print('User Report\n' + body)


    def _parseComment(self, comment, identifier):

        '''
            parses the comment to get the information after the identifier
            pre:  praw comment object, and the identifier
            post: a description is returned if there was one in the comment
                  returns None if one doesn't exist
        '''

        # Not the greatest regex will probably rework and make a better one
        regex = r'({})[\s\S]*'.format(identifier)
        search = re.compile(regex).search(str(comment))
        if search:
            # Try to get a description out of the commment
            description = search.group(0)[len(identifier) + 1:]
            # Remove anything extra after the newlines
            description = re.sub(r'\n[\s\S]+', '', description)

            # if it's an offer, we'll just get the money for the info
            if identifier == '!offer':
                regex = r'''[\$\£\€]?\-?([1-9]{1}[0-9]{0,2}(\,\d{3})*(\.\d{0,2})?|
                            [1-9]{1}\d{0,}(\.\d{0,2})?|0(\.\d{0,2})?|(\.\d{1,2}))|
                            ^\-?\$?([1-9]{1}\d{0,2}(\,\d{3})*(\.\d{0,2})?|
                            [1-9]{1}\d{0,}(\.\d{0,2})?|0(\.\d{0,2})?|(\.\d{1,2}))|
                            ^\(\$?([1-9]{1}\d{0,2}(\,\d{3})*(\.\d{0,2})?|
                            [1-9]{1}\d{0,}(\.\d{0,2})?|0(\.\d{0,2})?|(\.\d{1,2}))\)'''
                search = re.compile(regex).search(description)
                if search:
                    description = search.group(0)

            # Return none if there is no description
            return description if description else None


    def _formatResults(self, results):

        '''
            formats the results for a given users
            pre:  a results tuple or string
            post: a formatted string of the results is returned
        '''

        results_str = 'Here are some results from this user:\n\n----\n\n'
        # returns the default value if there was none, need to check
        # to see if it's the default, non-default is a tuple
        # probably should just have it return the default in a tuple
        if type(results) == str:
            return results
        # add the results in the tuple to the string
        for result in results:
            results_str += templates.RESULT.format(result[0], result[1])
            results_str += '\n\n'

        return results_str


    def _fetchUserInfo(self, user):

        '''
            gets the User's stats
            pre:  the username of the user 
            post: a formatted string of the users stats is returned
        '''

        user_info = self.db.getUserStats(user)
        user_stats = user_info['stats']
        user_results = user_info['results']

        s = templates.USER_STATS.format(user_stats[0],
                                        user_stats[1],
                                        user_stats[2],
                                        user_stats[3])


    def _shouldAddNewUserWarning(self, user):

        '''
            determines if a warning should be added because the user is new
            pre:  the username of the user
            post: returns True if the user is new, and False if not
        '''

        u_time = self.r.get_redditor(user).created_utc
        days = (datetime.utcnow() - datetime.fromtimestamp(u_time)).days
        return True if days < 30 else False


    def _getParentComment(self, cmt):

        '''
            pre:  the praw comment object you want to get the parent of
            post: the parent praw comment obeject is returned
        '''

        return self.r.get_info(thing_id=cmt.parent_id)
