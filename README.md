Reputation Manager Bot
=======================

Installations
-------------

This script requires python 3, praw, and praw-oauth2util.

To install praw type in your command line:

    pip install praw
    pip install praw-oauth2util

Reddit OAuth Setup
------------------

* [Go here on the account you want the bot to run on](https://www.reddit.com/prefs/apps/)
* Click on create a new app.
* Give it a name. 
* Select script from the radio buttons.
* Set the redirect uri to `http://127.0.0.1:65010/authorize_callback`
* After you create it you will be able to access the app key and secret.
* The **app key** is found here. Add this to the app_key variable in the oauth.txt file.

![here](http://i.imgur.com/7ybI5Fo.png) (**Do not give out to anybody**) 

* And the **app secret** here. Add this to the app_secret variable in the oauth.txt file.

![here](http://i.imgur.com/KkPE3EV.png) (**Do not give out to anybody**)

Note: On the first run of the bot you'll need to run it on a system that has a desktop environment.
So, if you're planning on running the bot on a server or VPS somewhere, run it on your computer
first. The first run will require you to authenticate it with the associated Reddit account by
opening a web page that looks like this:

![authentication page](http://i.imgur.com/se53uTq.png)

It will list different things it's allowed to do depending on the bots scope. After you
authenticate it, that page won't pop up again unless you change the OAuth credentials. And you'll
be free to run it on whatever environment you choose.

Config
------

Add the subreddit name (don't include the /r/) you want the bot to run in to the SUBREDDIT
variable in run_bot.py, and add the username of the bot to the USERNAME variable.

Usage
-----

To run the bot you will run the run_bot.py file. You can use the optional `-d` or `--debug` flag
to run the bot in debug mode. In debug mode the bot will not post any comments or send any messages
it will instead print out what it would submit.

If you're on an older version of the database, it may need updated (see the changelog to find out if you need to update). If you do need to update run the run_bot.py file with the `-m` or `--migrate` flag. This will run the database migration. You must have a migrate_db.py file in your directory to do this. After updating the database the bot will run as it would normally.

The bot has 7 identifiers:

* !offer
* !accept
* !success
* !scam
* !report
* !cancel
* !retract

#### Offer:

Usage: "!offer 10 dollars" or "!offer $10"

An offer to the OP must be present after the identifier. After the !offer is posted, the bot will
reply to the offerer with that user's stats, how many offers the user has completed, succedeed in, and scammed.
Along with that, a list of a maximum of 5 responses to how well the user did. These responses are optional
so not all users will end up having them. A warning is also added if the user's account is less than 2 months old.

Offers that posted after the thread's OP accepts an offer will be notified that an offer has already been accepted.

#### Accept:

Usage: "!accept" in reply to an !offer post.

An !accept comment must be in reply of an !offer comment. This will open up the thread and save the offer.
All other offers after an !accept has been made in a thread will be turned down.

#### Success:

Usage: "!success" or "!success John Doe did a wonderful job with my task."

A !success comment must be a top level comment in the thread, and can only be posted by the OP of the thread.
After the !success is posted, the user that made the accepted offer will get +1 added to their completions
and successes. If there is a response included, that will be added to the user's responses.

#### Scam:

Usage: "!scam" or "!scam John doe never gave my what I asked and took my money."

A !scam comment can be nested or a top level comment. When a !scam comment is made the User whose offer
was accepted gets +1 added to their scams. This will also report the user by sending a message to 
the mods of the subreddit.

#### Report:

Usage: "!report" or "!report This guy keeps harrasing me."

A !report comment can be made anywhere in the thread. A link to the comment will be sent to the mods
as well as any description given.

#### Cancel/Retract:

Usage: "!cancel" or "!retract"

A !cancel or !retract will cancel an accepted deal. The user that cancels, either the OP of the post or the accepted offerer will then have their retractions incremented 1.

License
-------

The MIT License (MIT)

Copyright (c) 2015 Nick Hurst

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
