import sqlite3

def migrate_db(sql, c):
    c.executescript('''CREATE TEMPORARY TABLE users_backup(id, uname, successes, scams);
                     INSERT INTO users_backup SELECT id,uname,successes,scams FROM users;
                     DROP TABLE users;
                     CREATE TABLE users(id INTEGER PRIMARY KEY AUTOINCREMENT,
                                        uname TEXT, 
                                        successes INTEGER DEFAULT 0,
                                        scams INTEGER DEFAULT 0,
                                        retractions INTEGER DEFAULT 0);
                    INSERT INTO users(id,uname,successes,scams) SELECT id,uname,successes,scams FROM users_backup;
                    DROP TABLE users_backup;
                    DROP TABLE results;''')
    sql.commit()
