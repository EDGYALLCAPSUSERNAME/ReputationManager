import sqlite3
import logging

# in case we don't need the migration .py anymore
try:
    from migrate_db import migrate_db
except ImportError:
    pass

class DatabaseManager:


    def __init__(self, db_name):

        '''
            initialize the DatabaseManager class
            pre:  name of the database
            post: database is created and initialized
        '''

        self.sql = sqlite3.connect(db_name)
        self.c = self.sql.cursor()

        # configure logging
        logging.basicConfig(filename='REPMLog.log',
                            level=logging.DEBUG,
                            format='%(asctime)s %(message)s')
        self._initDB()


    def migrate(self):
        migrate_db(self.sql, self.c)
        print('Database has successfully been updated.')


    def doesUserExistInDatabase(self, username):

        '''
            retrieve if the user already exists in the database
            pre:  username of the user you want to test
            post: returns boolean of whether or not the user exists in db
        '''

        try:
            self.c.execute('SELECT uname FROM users WHERE uname = ?', [username])
            if self.c.fetchone() == None:
                return False
            else:
                return True
        except sqlite3.Error as e:
            logging.debug('ERROR: Couldn\'t check if user exists. Reason: {}'.format(e))
            return False


    def addUserToTable(self, username):

        '''
            adds user to the users table
            pre:  username of user you want to add to the table
            post: returns True if successful, False otherwise
        '''

        try:
            self.c.execute('INSERT INTO users(uname) VALUES(?)', [username])
            self.sql.commit()
            return True
        except sqlite3.Error as e:
            logging.debug('ERROR: Could\'t insert user into database. Reason: {}'.format(e))
            return False


    def addFinishedResults(self, username, result):

        '''
            updates the users stats and updates the users results
            pre:  username, the result of the thread, and a reason if one was given
            post: returns boolean if it succeeded or not
        '''

        update_user = self._updateUser(username, result)
        if update_user:
            return True
        else:
            return False


    def getUserStats(self, username):

        '''
            retrieves the users stats and results from the db 
            pre:  username of the user you want the info for
            post: returns a dictionary containing a tuple of stats,
                  and a tuple of results if they exist, a string if not
        '''

        return self._getUser(username)


    def createThreadEntry(self, thread_id, accepted_by=None, created_by=None, offer=None):

        '''
            creates an entry for the thread
            pre:  the thread_id, accepted_by which is the user whos offer got accepted
                  created_by the original poster of the offer, and the offer accepted
            post: an entry for the thread is created, returns a booelan if it succeeded or not
        '''

        try:
            self.c.execute('''INSERT INTO threads(thread_id, accepted_by, created_by, offer) 
                              VALUES(?,?,?,?)''', [thread_id, accepted_by, created_by, offer])
            self.sql.commit()
            return True
        except sqlite3.Error as e:
            logging.debug('ERROR: Couldn\'t create thread entry. Reason: {}'.format(e))
            return False


    def completeThread(self, thread_id):

        '''
            completes the thread
            pre:  the thread_id of thread to be completed
            post: the completed column for the thread is set to 1, returns a boolean
                  of if it succeeded or not
        '''

        try:
            self.c.execute('UPDATE threads SET completed = 1 WHERE thread_id = ?', [thread_id])
            self.sql.commit()
            return True
        except sqlite3.Error as e:
            logging.debug('ERROR: Couldn\'t complete thread. Reason: {}'.format(e))
            return False


    def openThread(self, thread_id):

        '''
            if a previously completed thread needs reopened, set it back to 0
            pre:  the thread id of the thread to be opened
            post: the completed column is set to 0, returns a boolean
                  of if it succeeded or not
        '''

        try:
            self.c.execute('DELETE FROM threads WHERE thread_id = ?', [thread_id])
            self.sql.commit()
            return True
        except sqlite3.Error as e:
            logging.debug('ERROR: Couldn\'t open thread. Reason: {}'.format(e))
            return False


    def isThreadCompleted(self, thread_id):

        '''
            get the status of a threads completion
            pre:  the id of the thread
            post: returns True if it is completed, and False if it is not completed
        '''

        try:
            self.c.execute('SELECT completed FROM threads WHERE thread_id = ?', [thread_id])
            value = self.c.fetchone()
            if value:
                if value[0] == 0:
                    return False
                else:
                    return True
            else:
                return False
        except sqlite3.Error as e:
            logging.debug('ERROR: Couldn\'t check if thread is completed. Reason: {}'.format(e))
            return False


    def hasOfferBeenAccepted(self, thread_id):

        '''
            get if an offer has been accepted on the thread
            pre:  the thread id of the thread
            post: returns True if the offer has been accepted, false if not
        '''

        try:
            self.c.execute('SELECT accepted_by FROM threads WHERE thread_id = ?', [thread_id])
            value = self.c.fetchone()
            if value:
                return True
            else:
                return False
        except sqlite3.Error as e:
            logging.debug('ERROR: Couldn\'t check if offer has been accept. Reason: {}'.format(e))
            return False


    def getUserThatOffered(self, thread_id):

        '''
            retrieve the user whose offer has been accepted
            pre:  the id of the thread
            post: returns the name of the user whose offer was accepted, returns None if
                  there is no user
        '''

        try:
            self.c.execute('SELECT accepted_by FROM threads WHERE thread_id = ?', [thread_id])
            value = self.c.fetchone()
            if value:
                return value
            else:
                return None
        except sqlite3.Error as e:
            logging.debug('ERROR: Couldn\'t get user that offered. Reason: {}'.format(e))
            return False


    def addCommentToReplied(self, comment_id):

        '''
            adds the comment id to the replied table
            pre:  the id of the comment
            post: the id is added to the table, returns True on success, False otherwise
        '''

        try:
            self.c.execute('INSERT INTO replied VALUES(?)', [comment_id])
            self.sql.commit()
            return True
        except sqlite3.Error as e:
            logging.debug('ERROR: Couldn\'t add comment id to replied. Reason: {}'.format(e))
            return False


    def hasBotAlreadyReplied(self, comment_id):

        '''
            finds out if the bot has already replied to the comment
            pre:  comment id of the comment
            post: returns True if it has, False if it hasn't replied
        '''

        self.c.execute('SELECT * FROM replied WHERE comment_id = ?', [comment_id])
        if self.c.fetchone():
            return True
        else:
            return False


    # "Private" methods


    def _initDB(self):

        '''
            initializer for the database tables
            pre:  None
            post: 4 tables are created if they don't already exist
        '''

        self.c.execute('''CREATE TABLE IF NOT EXISTS users
                          (id INTEGER PRIMARY KEY AUTOINCREMENT,
                           uname TEXT,
                           successes INTEGER DEFAULT 0,
                           retractions INTEGER DEFAULT 0,
                           scams INTEGER DEFAULT 0)''')
        self.c.execute('''CREATE TABLE IF NOT EXISTS threads
                          (id INTEGER PRIMARY KEY AUTOINCREMENT,
                           thread_id TEXT,
                           accepted_by TEXT,
                           created_by TEXT,
                           offer TEXT,
                           completed INTEGER DEFAULT 0)''')
        self.c.execute('CREATE TABLE IF NOT EXISTS replied(comment_id TEXT)')
        self.sql.commit()


    def _getUser(self, username):

        '''
            get the users stats
            pre:  name of the user
            post: a tuple of the users stats, if they user doesn't exist in db 
                  calls addUserToTable and creates the user
        '''

        try:
            self.c.execute('''SELECT uname, successes, retractions,
                              scams FROM users WHERE uname = ?''', [username])
            user = self.c.fetchone()
            if not user:
                # add them to the db and assign default stats
                self.addUserToTable(username)
                user = (username, 0, 0, 0)
            return user
        except sqlite3.Error as e:
            logging.debug('ERROR: Could\'t get user from DB. Reason: {}'.format(e))
            return False


    def _getUserDatabaseID(self, username):

        '''
            gets the database id of the user
            pre:  username of user
            post: returns the database id of the user requested, False if it doesn't
                  exist
        '''

        try:
            self.c.execute('SELECT id FROM users WHERE uname = ?', [username])
            u_id = self.c.fetchone()
            return u_id[0] if u_id else False
        except sqlite3.Error as e:
            logging.debug('ERROR: Couldn\'t get u_id. Reason: {}'.format(e))
            return False


    def _updateUser(self, username, result):

        '''
            updates the stats of the user
            pre:  the username of the user, and the result of the thread
            post: the users stats are updated based upon the result, returns True
                  if it succeeded false if it didn't
        '''

        try:
            if result == 'success':
                self.c.execute('''UPDATE users 
                                  SET successes = successes + 1
                                  WHERE uname = ?''', [username])
            elif result == 'cancel':
                self.c.execute('''UPDATE users 
                                  SET retractions = retractions + 1
                                  WHERE uname = ?''', [username])
            elif result == 'scam':
                self.c.execute('''UPDATE users 
                                  SET scams = scams + 1 
                                  WHERE uname = ?''', [username])

            self.sql.commit()
            return True
        except sqlite3.Error as e:
            logging.debug('ERROR: Couln\'t update users stats. Reason: {}'.format(e))
            return False
