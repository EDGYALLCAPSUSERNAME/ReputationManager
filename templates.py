USER_STATS = """
|Username|Successes|Retractions|Scams|
:-------:|:-------:|:---------:|:----:
|{}|{}|{}|{}|
"""

RESULT = """

**{}**: {}

----
"""

ACCEPTED = """
/u/{} has accepted /u/{}'s offer which is {}.
"""

COMPLETED = { 
    'success': """
The deal has been a success. /u/{}'s profile has been updated.
""",
    'scam': """
The deal has not been a success. /u/{}'s profile has been updated, and has been reported.
""",
    'cancel': """
The deal has been cancelled. /u/{}'s profile has been updated.
""" }

ERROR = {
    'err': """
Something happened. Hopefully you're not a user, because you're not supposed to see this. 
A mod can help you out shortly.
""",
    'NoParent': """
This comment doesn't have any parents, so there's nothing I can do.
"""

}

WARNING = """
**Warning: this account is new (< 2 months old)**
"""