import argparse
from ReputationManager import ReputationManager

# User config
# --------------------------------------------------------
SUBREDDIT = ''
USERNAME = ''
# --------------------------------------------------------

# parser for debug mode
parser = argparse.ArgumentParser()
parser.add_argument('-d', '--debug', action='store_true',
                    help='Run the bot in debug mode. Will not reply to comments, only print to console')
parser.add_argument('-m', '--migrate', action='store_true',
                    help='Runs a database migration before running the bot')
args = parser.parse_args()


# try to import my testing settings
try:
    import bot
    SUBREDDIT = bot.SUBREDDIT
    USERNAME = bot.USERNAME
except ImportError:
    pass

def main():
    if args.migrate and args.debug:
        rm = ReputationManager(SUBREDDIT, USERNAME, 'database.db', debug=True)
        print('Running migration...')
        rm.db.migrate()
    elif args.migrate:
        rm = ReputationManager(SUBREDDIT, USERNAME, 'database.db', debug=False)
        print('Running migration...')
        rm.db.migrate()
    elif args.debug:
        rm = ReputationManager(SUBREDDIT, USERNAME, 'database.db', debug=True)
    else:
        rm = ReputationManager(SUBREDDIT, USERNAME, 'database.db', debug=False)

    try:
        rm.mainLoop()
    except KeyboardInterrupt:
        print('Exiting...')

if __name__ == '__main__':
    main()
